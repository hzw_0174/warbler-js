/*
 * @Author: 一尾流莺
 * @Description:操作字符串相关的方法
 * @Date: 2021-09-10 11:15:49
 * @LastEditTime: 2021-09-10 14:59:49
 * @FilePath: \warblerJS\src\string\index.js
 */

// 反转字符串
import reverseString from './reverseString';

// 将字符串的首字母转换成大写字母
import toUpperFirstLetter from './toUpperFirstLetter';

// 将字符串的首字母转换成大写字母
import toLowerFirstLetter from './toLowerFirstLetter';

export {
  reverseString,
  toUpperFirstLetter,
  toLowerFirstLetter,
};
