/*
 * @Author: 一尾流莺
 * @Description:操作数字相关的方法
 * @Date: 2021-09-10 11:15:38
 * @LastEditTime: 2021-09-15 11:01:15
 * @FilePath: \warblerJS\src\number\index.js
 */

// 判断一个数字是偶数还是奇数
import isEvenNumber from './isEvenNumber';

// 获取参数的平均数值
import getAverage from './getAverage';

// 判断数字是否可以整除
import isDivisible from './isDivisible';

export {
  isEvenNumber,
  getAverage,
  isDivisible,
};
