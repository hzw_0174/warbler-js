/*
 * @Author: 一尾流莺
 * @Description:常用方法
 * @Date: 2021-09-10 11:44:18
 * @LastEditTime: 2021-09-14 15:36:00
 * @FilePath: \warblerJS\src\methods\index.js
 */

// 模拟延迟
import imitateDelay from './imitateDelay';
// 异步加载script脚本
import loadScript from './loadScript';

export {
  imitateDelay,
  loadScript,
};
