/*
 * @Author: 一尾流莺
 * @Description:入口文件
 * @Date: 2021-09-10 11:14:16
 * @LastEditTime: 2021-09-15 11:01:36
 * @FilePath: \warblerJS\src\index.js
 */

// 数组方法
export {
  findObjectInArray,
  arrWithoutDupli,
  arrObjectWithoutDupli,
  countFrequency,
  countObjFrequency,
  filterUnique,
  filterNoUnique,
} from './array/index';

// boolean值方法
export {
  randomBoolean,
} from './boolean/index';

// 日期方法
export {
  dayOfYear,
} from './date/index';

// 函数方法
export {

} from './function/index';

// 常用功能方法
export {
  imitateDelay,
  loadScript,
} from './methods/index';

// 数字方法
export {
  isEvenNumber,
  getAverage,
  isDivisible,
} from './number/index';

// 对象方法
export {

} from './object/index';

// 字符串方法
export {
  reverseString,
  toUpperFirstLetter,
  toLowerFirstLetter,
} from './string/index';
