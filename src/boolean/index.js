/*
 * @Author: 一尾流莺
 * @Description:
 * @Date: 2021-09-10 14:52:45
 * @LastEditTime: 2021-09-10 14:52:45
 * @FilePath: \warblerJS\src\boolean\index.js
 */

// 获得一个随机的布尔值（true/false）
import randomBoolean from './randomBoolean';

export {
  randomBoolean,
};
